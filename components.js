class Comment extends React.Component {
  render() {
    return (
      <div className="comment">
      <h3>{this.props.author}</h3>
      <p>{this.props.body}</p>
      </div>
    );
  }
}

class CommentBox extends React.Component {

  _getComments() {
    const commentList = [
      { id: 1, author: 'Morgan McCircuit', body: 'Great picture!' },
      { id: 2, author: 'Bending Bender', body: 'Excellent stuff' }
   ];

   return commentList.map((comment)=>{
     return(
       <Comment author={comment.author} body={comment.body} key={comment.id}/>
     );
   });

  }

  render() {
    const comments = this._getComments() || [];
    return(
      <div>
      <h1>Comments Box</h1>
      <h2>Total {comments.length} Comments</h2>
      {comments}
      </div>

    );
  }
}

ReactDOM.render(
  <CommentBox />,
  document.getElementById('story-app')
);
